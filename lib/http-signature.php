<?php

/**
 * Creates signed headers according to HTTP Signature Scheme.
 *
 * @link https://github.com/joyent/node-http-signature/blob/master/http_signing
 *
 * @param string Private key (content or filename)
 * @param string Key ID for Authorization header
 * @param array Additional headers to sign. 
 * @param string Algorithm. Default: 'rsa-sha256'
 * @return array Headers for HTTP request.
 *    Contains by default keys 'date', 'Authorization'.
 */
function http_signature($key, $keyId, $headers = array(), $algorithm = '')
{
  $defaultHeaders = array(
    'date' => @date("r")
  );
  $defaultAlgorithm = 'rsa-sha256';

  if (empty($key))
    throw new Exception("Please specify key");

  if (empty($keyId))
    throw new Exception("Please specify keyId");

  if (! is_array($headers))
    $headers = array();
  $headers = array_merge($defaultHeaders, $headers);

  if (empty($algorithm))
    $algorithm = $defaultAlgorithm;

  $signHeaders = array();
  foreach ($headers as $k => $v) {
    $signHeaders[] = $k.': '.$v;
  }
  $signHeaders = implode("\n", $signHeaders);

  if (is_file($key)) {
    $fp = fopen($key, "r");
    $keyContent = fread($fp, 8192);
    fclose($fp);
    $key = $keyContent;
  }
  $privKeyId = @openssl_get_privatekey($key);
  if (! is_resource($privKeyId))
    throw new Exception('Please provide correct private key');

  $signatureAlg = null;
  switch ($algorithm) {
    case 'rsa-sha256':
      $signatureAlg = OPENSSL_ALGO_SHA256;
      break;
  }

  $signature = null;
  openssl_sign($signHeaders, $signature, $privKeyId, $signatureAlg);
  openssl_free_key($privKeyId);
  
  $headers['Authorization'] = sprintf(
    "Signature keyId=\"%s\",algorithm=\"%s\",headers=\"%s\",signature=\"%s\"",
    $keyId, $algorithm, implode(' ', array_keys($headers)), base64_encode($signature)
  );

  return $headers;
}

?>
