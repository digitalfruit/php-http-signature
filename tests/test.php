<?php

require_once __DIR__ . '/../vendor/autoload.php';

$key = dirname(__FILE__)."/keys/foo-key.pem";
$keyId = 'foo-key';
$data = json_encode(array('foo' => 'bar'));

$headers = array('content-md5' => md5($data), 'request-line' => 'GET /foo HTTP/1.1');

try {
  $signedHeaders = http_signature($key, $keyId, $headers);
  print_r($signedHeaders);
} catch (Exception $e) {
  echo $e->getMessage();
}

?>
